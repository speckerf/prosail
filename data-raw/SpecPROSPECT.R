## code to prepare `SpecPROSPECT` dataset goes here
SpecPROSPECT <- prospect::SpecPROSPECT


usethis::use_data(SpecPROSPECT, overwrite = TRUE)
