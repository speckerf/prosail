Package: prosail
Title: PROSAIL leaf and canopy radiative transfer model and inversion routines
Version: 1.2.3
Authors@R: c(person(given = "Jean-Baptiste", 
					    family = "Feret", 
					    email = "jb.feret@teledetection.fr", 
					    role = c("aut", "cre", "cph"),
					    comment = c(ORCID = "0000-0002-0151-1334")),
			    person(	given = "Florian", 
					    family = "de Boissieu", 
					    email = "florian.deboissieu@irstea.fr", 
					    role = c("aut"), 
					    comment = c(ORCID = "0000-0002-2185-9952")))
Description: The package prosail includes the canopy radiative transfer model PROSAIL coupled with the leaf model PROSPECT-PRO, the latest version of the PROSPECT leaf model. 
              It also includes inversion routines based on iterative optimization and hybrid method in order to estimate vegetation properties (leaf and canopy) from sensor measurements.
License: GPL-3
Depends: R (>= 3.5.0)
Encoding: UTF-8
LazyData: true
Roxygen: list(markdown = TRUE)
RoxygenNote: 7.2.3
Suggests: knitr,
    rmarkdown
Imports: 
    caret,
    dplyr,
    expandFunctions,
    ggplot2,
    matrixStats,
    methods,
    pracma,
    raster,
    rgdal,
    simsalapar,
    stars,
    stringr,
    utils,
    progress,
    prospect,
    XML
Remotes: gitlab::jbferet/prospect
VignetteBuilder: knitr
URL: https://gitlab.com/jbferet/prosail
BugReports: https://gitlab.com/jbferet/prosail/issues
